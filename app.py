import discord
from discord.ext import commands
import youtube_dl
import os
import asyncio

client = commands.Bot(command_prefix="!")

song_queue = []

async def handleMusic(ctx, url: str):
    isPlaying = os.path.isfile("song.mp3")
    try:
        if isPlaying:
            os.remove("song.mp3")
    except PermissionError:
        await ctx.send("Merci d'attendre que la musique se termine")
        return

    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    voiceChannel = discord.utils.get(ctx.guild.voice_channels, name='General')

    if voice is None or not voice.is_connected():
        await voiceChannel.connect()

    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

    for file in os.listdir("./"):
        if file.endswith(".mp3"): os.rename(file, "song.mp3")

    audio_source = discord.FFmpegPCMAudio("song.mp3")

    return [voice, audio_source]


@client.command()
async def play(ctx, url : str):
    if len(song_queue) > 0:
        song_queue.append(url)
        await ctx.send('Votre musique a bien ajoutée en fil d\'attente')
    else: 
        musicData = await handleMusic(ctx, url)
        song_queue.append(url)
        musicData[0].play(musicData[1], after=lambda e: asyncio.run_coroutine_threadsafe(song_end(ctx), client.loop))
        await ctx.send('Votre musique a bien été jouée')

@client.command()
async def leave(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice is None: return

    if voice.is_connected():
        await voice.disconnect()
    else:
        await ctx.send("Le bot n'est pas connecté à un salon vocal.")


@client.command()
async def pause(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice is None: return

    if voice.is_playing():
        voice.pause()
        await ctx.send("Musique mis en pause.")
    else:
        await ctx.send("Aucune musique n'est en cours.")


@client.command()
async def resume(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice is None: return

    if voice.is_paused():
        voice.resume()
        await ctx.send("Musique reprise")
    else:
        await ctx.send("La musique n'est pas en pause")


@client.command()
async def stop(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice is None: return
    
    voice.stop()

async def song_end(ctx):
    song_queue.pop(0)
    try:
        if len(song_queue) > 0:
            musicData = await handleMusic(ctx, song_queue[0])
            musicData[0].play(musicData[1], after=lambda e: asyncio.run_coroutine_threadsafe(song_end(ctx), client.loop))

        else:
            voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
            if voice is None: return

            if voice.is_connected():
                await voice.disconnect()
    except IndexError :
        pass


client.run('Njk5OTcwNTQzODcyNTA3OTc1.XpcIXg.vxwcGxd7xKuIUdlJaBd70ZwgV6Q')
